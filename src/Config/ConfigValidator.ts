import { existsSync } from 'node:fs';

export default class ConfigValidator {
  public readonly _errors: string[] = [];

  get errors(): string[] {
    return this._errors;
  }

  validate(preferences: Preferences): boolean {
    const cliPath: string | undefined =
      preferences.cliPath ||
      ['/usr/local/bin/docker', '/opt/homebrew/bin/docker'].find((path: string): boolean => existsSync(path));

    if (!cliPath) {
      this.errors.push('Docker CLI not found');
    }

    if (!existsSync(preferences.directoryPath)) {
      this.errors.push('Directory path not found');
    }

    return !this.errors.length;
  }
}

import { Clipboard, closeMainWindow, getPreferenceValues, showToast, Toast } from '@raycast/api';
import { v2 as compose } from 'docker-compose';
import ConfigValidator from './Config/ConfigValidator.js';

export default async (): Promise<void> => {
  const preferences: Preferences = getPreferenceValues<Preferences>();

  const configValidator: ConfigValidator = new ConfigValidator();

  if (!configValidator.validate(preferences)) {
    await showToast({
      title: configValidator.errors[0] ?? 'Config validation error',
      style: Toast.Style.Failure,
    });

    console.error('Error parsing config: ', configValidator.errors);

    return;
  }

  await closeMainWindow();

  const command: string[] = [
    '/var/www/craft/console.sh',
    '--client=develop',
    'usermanagement_localdevtoken',
    'index',
    '1',
    '31449600',
  ];

  await compose
    .exec('php8', command.join(' '), {
      cwd: preferences.directoryPath,
      executable: {
        executablePath: preferences.cliPath,
      },
    })
    .then(async (result: compose.IDockerComposeResult): Promise<void> => {
      const token: string = result.out.substring(result.out.lastIndexOf('document.cookie')).trim();

      await Clipboard.copy(token);

      await showToast({
        title: 'Token copied to clipboard',
      });

      console.log(token);
    })
    .catch(async (error): Promise<void> => {
      console.log(error);

      await showToast({
        title: error.err || error.out.slice(0, error.out.indexOf('\n')) || 'Script failed to run',
        style: Toast.Style.Failure,
      });
    });
};

dev: develop

develop: watch

watch:
	npx ray develop

build:
	npx ray build -e dist

lint:
	docker run --rm -it \
		--volume=./:/app \
		--workdir=/app \
		node:20.11.0-alpine3.19 \
		npx --no-update-notifier eslint .

fmt:
	docker run --rm -it \
		--volume=./:/app \
		--workdir=/app \
		node:20.11.0-alpine3.19 \
		npx --no-update-notifier prettier . --write
